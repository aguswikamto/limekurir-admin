import React from 'react'
import { CFooter } from '@coreui/react'

const AppFooter = () => {
  return (
    <CFooter>
      <div>
        <a href="https://www.limecommerce.com/" target="_blank" rel="noopener noreferrer">
          LimeKurir
        </a>
        <span className="ms-1">&copy; 2022</span>
      </div>
      <div className="ms-auto">
        <span className="me-1">Powered by</span>
        <a href="https://www.limecommerce.com/" target="_blank" rel="noopener noreferrer">
          Limecommerce
        </a>
      </div>
    </CFooter>
  )
}

export default React.memo(AppFooter)
